// This Script once setup, auto forwards all messages from a chat to another.
var tg_bot_token = "" // Replace with Telegram Bot Token
var admin = "5854304441" // to get logs or error messages
var from_channel = [-1001845572485] // add as many as you want in this
var to_channel = [-1001549539244] // Don't add too many. upto 1-10 should work fine. Recommended is 5 Max.
var debug = false // Debug Mode will give alot of Messages based on messages recieved in first channel.

async function handleRequest(request) {
    var url = new URL(request.url);
    var path = url.pathname;
    var hostname = url.hostname;
    if (path == '/') {
        var setwebhook = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/setWebhook?url=https://" + hostname + "/bot&drop_pending_updates=true&max_connections=100", {
            method: "GET",
        });
        if (setwebhook.ok) {
            return new Response("Web Hook Set", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        } else {
            return new Response("Unable to Set Web Hook, something went wrong.", {
                headers: {
                    'content-type': 'text/html;charset=UTF-8',
                },
            });
        }
    } else if (path == '/bot') {
        var data = JSON.stringify(await request.json());
        var obj = JSON.parse(data);
        if (debug) {
            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Incoming request from " + from_channel + " : " + data, {
                method: "GET",
            });
        }
        if (obj.hasOwnProperty('channel_post')) {
            if (from_channel.includes(obj.channel_post.sender_chat.id)) {
                for (let i = 0; i < to_channel.length; i++) {
                    console.log("runtime: " + i)
                    var res = await fetch("https://api.telegram.org/bot" + tg_bot_token + "/copyMessage?disable_web_page_preview=true&chat_id=" + to_channel[i] + "&from_chat_id=" + from_channel + "&message_id=" + obj.channel_post.message_id, {
                        method: "GET",
                    });
                    var jsondata = await res.text();
                    var copydata1 = JSON.parse(jsondata);
                    if (res.ok) {
                        console.log("Incoming Message Copied.")
                        if (debug) {
                            await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=RES OK: " + from_channel + " : " + jsondata, {
                                method: "GET",
                            });
                        }
                    } else if (res.status == 429) {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Flood Wait.\n\nRetry after " + copydata1.parameters.retry_after + " Seconds.", {
                            method: "GET",
                        });
                        return new Response(jsondata, {
                            status: 429,
                            headers: {
                                'content-type': 'application/json',
                            },
                        });
                    } else {
                        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + admin + "&text=Problem at Channel (not 429) " + from_channel + " : " + jsondata, {
                            method: "GET",
                        });
                    }
                }
                return new Response("OK!", {
                    status: 200,
                    headers: {
                        'content-type': 'application/json',
                    },
                });
            }
        } else if (obj.hasOwnProperty('message')) {
            if (obj.message.hasOwnProperty('text')) {
                var usertext = obj.message.text
                if (usertext.startsWith('/start')) {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=I'm already UP Boss!", {
                        method: "GET",
                    });
                } else {
                    await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id=" + obj.message.from.id + "&reply_to_message_id=" + obj.message.message_id + "&text=Bhadoo Cloner Bot Private Server.", {
                        method: "GET",
                    });
                }
            }
        }
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    } else {
        return new Response("OK", {
            status: 200,
            headers: {
                'content-type': 'application/json',
            },
        });
    }
}

addEventListener('fetch', event => {
    return event.respondWith(handleRequest(event.request));
});
